var MS_PER_SECOND = 1000
var MS_PER_MINUTE = MS_PER_SECOND * 60
var MS_PER_HOUR = MS_PER_MINUTE * 60
var MS_PER_DAY = MS_PER_HOUR * 24;

function startCountdown(year, month, day, hour, minute, utcDelta, textView) {
	var future = new Date(
		new Date(Date.UTC(year, month - 1, day, hour, minute)).getTime() - utcDelta * MS_PER_HOUR
	)
	console.log(future);
	var divDays = addDivWithClass(textView, 'days');
	var divHours = addDivWithClass(textView, 'hours');
	var divMins = addDivWithClass(textView, 'mins');
	var divSecs = addDivWithClass(textView, 'secs');

	setInterval(function() {
		displayDiff(future, divDays, divHours, divMins, divSecs)
	}, 75);
}

function addDivWithClass(view, className) {
	var result = document.createElement('div');
	result.className = className;
	view.appendChild(result);
	return result;
}

function displayDiff(future, divDays, divHours, divMins, divSecs) {
	var now = new Date();
	var msecs = future.getTime() - now.getTime();

	if (msecs < 0) {
		divDays.innerHTML = 0
		divHours.innerHTML = pad(0, 2);
		divMins.innerHTML = pad(0, 2);
		divSecs.innerHTML = pad(0, 2);
		return;		
	}

	var days = Math.floor(msecs / MS_PER_DAY)
	msecs -= days * MS_PER_DAY;
	var hours = Math.floor(msecs / MS_PER_HOUR)
	msecs -= hours * MS_PER_HOUR;
	var minutes = Math.floor(msecs / MS_PER_MINUTE)
	msecs -= minutes * MS_PER_MINUTE
	var seconds = Math.floor(msecs / MS_PER_SECOND)
	divDays.innerHTML = days;
	divHours.innerHTML = pad(hours, 2);
	divMins.innerHTML = pad(minutes, 2);
	divSecs.innerHTML = pad(seconds, 2);
}

// pad number with left zeros
function pad(number, length) {
	number = number.toString();
	return "0".repeat(length - number.length) + number;
}
